<?php

/**
 * @var \CMain $APPLICATION
 */

if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Page\Asset;

$asset = Asset::getInstance();

$asset->addCss('/node_modules/bootstrap/dist/css/bootstrap.min.css');
$asset->addCss('/node_modules/font-awesome/css/font-awesome.min.css');

$asset->addJs('/node_modules/jquery/dist/jquery.min.js');
$asset->addJs('/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js');
$asset->addJs('/js/app.js');
$asset->addJs(SITE_TEMPLATE_PATH . '/js/layout.js');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?= SITE_CHARSET; ?>">

    <meta name="viewport" content="width=device-width, user-scalable=no, shrink-to-fit=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.png">

    <title><?php $APPLICATION->ShowTitle(); ?></title>

    <?php $APPLICATION->ShowHead(); ?>
</head>
<body>

<div id="panel">
    <?php $APPLICATION->ShowPanel(); ?>
</div>

<div class="layout-wrapper">
    <header>

    </header>

    <div class="page-wrapper">
