<?php

if (!\defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$TEMPLATE['default.php'] = Array(
    'name' => 'Default page',
    'sort' => 1,
);

$TEMPLATE['empty.php'] = Array(
    'name' => 'Empty page',
    'sort' => 2,
);
