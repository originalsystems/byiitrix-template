<?php

/**
 * @var CMain $APPLICATION
 */

use app\EventHandler;

global $APPLICATION;

require \dirname(\dirname(\dirname(__DIR__))) . '/vendor/autoload.php';

\Dotenv\Dotenv::create(\dirname(\dirname(\dirname(__DIR__))))->load();

require \dirname(\dirname(\dirname(__DIR__))) . '/common/config/define.php';
require APP_ROOT . '/common/Yii.php';
require APP_ROOT . '/common/config/bootstrap.php';

\CModule::IncludeModule('iblock');

if (PHP_SAPI !== 'cli') {
    if (\stripos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) {
        \http_response_code(301);
        \header('Location: /old-browser/');

        exit;
    }
}

\AddEventHandler('main', 'OnEndBufferContent', function (&$content) {

});

/*
 * EVENTS
 */
\AddEventHandler('iblock', 'OnBeforeIBlockElementAdd', [EventHandler::class, 'OnBeforeIBlockElementAdd']);
//\AddEventHandler('iblock', 'OnAfterIBlockElementAdd', [EventHandler::class, 'OnAfterIBlockElementAdd']);
//\AddEventHandler('iblock', 'OnBeforeIBlockElementUpdate', [EventHandler::class, 'OnBeforeIBlockElementUpdate']);
//\AddEventHandler('iblock', 'OnAfterIBlockElementUpdate', [EventHandler::class, 'OnAfterIBlockElementUpdate']);
//\AddEventHandler('iblock', 'OnBeforeIBlockElementDelete', [EventHandler::class, 'OnBeforeIBlockElementDelete']);
//\AddEventHandler('iblock', 'OnAfterIBlockElementDelete', [EventHandler::class, 'OnAfterIBlockElementDelete']);

//\AddEventHandler('iblock', 'OnBeforeIBlockSectionAdd', [EventHandler::class, 'OnBeforeIBlockSectionAdd']);
//\AddEventHandler('iblock', 'OnAfterIBlockSectionAdd', [EventHandler::class, 'OnAfterIBlockSectionAdd']);
//\AddEventHandler('iblock', 'OnBeforeIBlockSectionUpdate', [EventHandler::class, 'OnBeforeIBlockSectionUpdate']);
//\AddEventHandler('iblock', 'OnAfterIBlockSectionUpdate', [EventHandler::class, 'OnAfterIBlockSectionUpdate']);
//\AddEventHandler('iblock', 'OnBeforeIBlockSectionDelete', [EventHandler::class, 'OnBeforeIBlockSectionDelete']);
//\AddEventHandler('iblock', 'OnAfterIBlockSectionDelete', [EventHandler::class, 'OnAfterIBlockSectionDelete']);

//\AddEventHandler('catalog', 'OnBeforeCatalogImport1C', [EventHandler::class, 'OnBeforeCatalogImport1C']);
//\AddEventHandler('catalog', 'OnSuccessCatalogImport1C', [EventHandler::class, 'OnSuccessCatalogImport1C']);

function buildNestedMenu($items)
{
    $menu   = [];
    $first  = \current($items);
    $level  = (int) $first['DEPTH_LEVEL'];
    $levels = [
        $level => &$menu,
    ];

    foreach ($items as $item) {
        $current = (int) $item['DEPTH_LEVEL'];

        $item['CHILDREN'] = [];

        if ($current > $level) {
            $levels[$current] = &$levels[$level][\count($levels[$level]) - 1]['CHILDREN'];
        }

        $levels[$current][] = $item;

        $level = $current;
    }

    unset($level, $levels, $current);

    return $menu;
}
