<?php

namespace app;

class EventHandler
{
    /**
     * @param array $arFields
     *
     * @return bool
     */
    public static function OnBeforeIBlockElementAdd(&$arFields)
    {
       self::debug(['arFields' => $arFields], 'bitrix.' . __FUNCTION__);

        /** @noinspection DegradedSwitchInspection */
        switch ((int)$arFields['IBLOCK_ID']) {
            default:
                // nothing to do
        }

        return true;
    }

    /**
     * @param array $arFields
     */
    public static function OnAfterIBlockElementAdd($arFields)
    {
       self::debug(['arFields' => $arFields], 'bitrix.' . __FUNCTION__);

        if (empty($arFields['ID'])) {
            return;
        }

        /** @noinspection DegradedSwitchInspection */
        switch ((int)$arFields['IBLOCK_ID']) {
            default:
                // nothing to do
        }
    }

    /**
     * @param array $arFields
     *
     * @return bool
     */
    public static function OnBeforeIBlockElementUpdate(&$arFields)
    {
       self::debug(['arFields' => $arFields], 'bitrix.' . __FUNCTION__);

        /** @noinspection DegradedSwitchInspection */
        switch ((int)$arFields['IBLOCK_ID']) {
            default:
                // nothing to do
        }

        return true;
    }

    /**
     * @param array $arFields
     */
    public static function OnAfterIBlockElementUpdate($arFields)
    {
       self::debug(['arFields' => $arFields], 'bitrix.' . __FUNCTION__);

        /** @noinspection DegradedSwitchInspection */
        switch ((int)$arFields['IBLOCK_ID']) {
            default:
                // nothing to do
        }
    }

    /**
     * @param integer $ID
     *
     * @return bool
     */
    public static function OnBeforeIBlockElementDelete($ID)
    {
       self::debug(['ID' => $ID], 'bitrix.' . __FUNCTION__);

        return true;
    }

    /**
     * @param array $arFields
     */
    public static function OnAfterIBlockElementDelete($arFields)
    {
       self::debug(['arFields' => $arFields], 'bitrix.' . __FUNCTION__);
    }

    /**
     * @param array $arFields
     *
     * @return bool
     */
    public static function OnBeforeIBlockSectionAdd(&$arFields)
    {
       self::debug(['arFields' => $arFields], 'bitrix.' . __FUNCTION__);

        /** @noinspection DegradedSwitchInspection */
        switch ((int)$arFields['IBLOCK_ID']) {
            default:
                // nothing to do
        }

        return true;
    }

    /**
     * @param array $arFields
     */
    public static function OnAfterIBlockSectionAdd($arFields)
    {
       self::debug(['arFields' => $arFields], 'bitrix.' . __FUNCTION__);

        /** @noinspection DegradedSwitchInspection */
        switch ((int)$arFields['IBLOCK_ID']) {
            default:
                // nothing to do
        }
    }

    /**
     * @param array $arFields
     *
     * @return bool
     */
    public static function OnBeforeIBlockSectionUpdate(&$arFields)
    {
       self::debug(['arFields' => $arFields], 'bitrix.' . __FUNCTION__);

        /** @noinspection DegradedSwitchInspection */
        switch ((int)$arFields['IBLOCK_ID']) {
            default:
                // nothing to do
        }

        return true;
    }

    /**
     * @param array $arFields
     */
    public static function OnAfterIBlockSectionUpdate($arFields)
    {
       self::debug(['arFields' => $arFields], 'bitrix.' . __FUNCTION__);

        /** @noinspection DegradedSwitchInspection */
        switch ((int)$arFields['IBLOCK_ID']) {
            default:
                // nothing to do
        }
    }

    /**
     * @param integer $ID
     *
     * @return bool
     */
    public static function OnBeforeIBlockSectionDelete($ID)
    {
       self::debug(['ID' => $ID], 'bitrix.' . __FUNCTION__);

        return true;
    }

    /**
     * @param array $arFields
     */
    public static function OnAfterIBlockSectionDelete($arFields)
    {
       self::debug(['arFields' => $arFields], 'bitrix.' . __FUNCTION__);

        /** @noinspection DegradedSwitchInspection */
        switch ((int)$arFields['IBLOCK_ID']) {
            default:
                // nothing to do
        }
    }

    /**
     * @param array  $arFields
     * @param string $xmlFile
     *
     * @return string|null
     */
    public static function OnBeforeCatalogImport1C($arFields, $xmlFile)
    {
       self::debug(['arFields' => $arFields, 'xmlFile' => $xmlFile], 'bitrix.' . __FUNCTION__);
    }

    /**
     * @param array  $arFields
     * @param string $xmlFile
     *
     * @return string|null
     */
    public static function OnSuccessCatalogImport1C($arFields, $xmlFile)
    {
       self::debug(['arFields' => $arFields, 'xmlFile' => $xmlFile], 'bitrix.' . __FUNCTION__);
    }

    private static function debug($data, string $name): void
    {
        if (YII_ENV_DEV) {
            $dir = APP_ROOT . '/common/runtime/logs';

            @mkdir($dir, BX_DIR_PERMISSIONS);

            file_put_contents($dir . '/' . $name . '.log', '[' . date('Y-m-d H:i:s') . ']: ' . var_export($data, true) . PHP_EOL, FILE_APPEND);
        }
    }
}
