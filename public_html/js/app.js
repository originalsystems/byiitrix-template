(function() {
    'use strict';

    window.App = {
        values(form) {
            let $form  = jQuery(form),
                values = {};

            $form.serializeArray().forEach(({name, value}) => {
                if (!name) {
                    return;
                }
                
                if (values[name]) {
                    if (Array.isArray(values[name]) === false) {
                        values[name] = [values[name]];
                    }

                    values[name].push(value);
                } else {
                    values[name] = value;
                }
            });

            return values;
        },

        ajax(component) {
            return (action, data) => {
                let form_data;

                if (typeof component === 'string') {
                    form_data = {component};
                } else {
                    form_data = this.values(component);
                }

                if (data === null || typeof data !== 'object') {
                    data = {};
                }

                form_data = jQuery.extend(true, form_data, data, {action});

                return new Promise((resolve, reject) => {
                    jQuery.ajax({
                        type:     'post',
                        dataType: 'json',
                        data:     form_data,
                        success:  (json) => json.success === false ? reject(json.errors) : resolve(json.data),
                        error:    () => reject(['Unexpected error'])
                    });
                });
            };
        }
    };
})();
