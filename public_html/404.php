<?php

/**
 * @var \CMain $APPLICATION
 */

include_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php';

\CHTTP::SetStatus('404 Not Found');

@\define('ERROR_404', 'Y');

require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php';

$APPLICATION->SetTitle('404 Not Found');
?>

<div class="page-404">
    <div class="container">
        <p>Something went wrong...</p>
    </div>
</div>

<?php require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'; ?>
