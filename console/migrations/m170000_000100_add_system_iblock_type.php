<?php

declare(strict_types=1);

use yii\db\Migration;
use app\Codex;

/** @noinspection LongInheritanceChainInspection */
class m170000_000100_add_system_iblock_type extends Migration
{
    public function safeUp(): bool
    {
        $type   = new \CIBlockType();
        $typeID = $type->Add([
            'ID'       => Codex::TYPE_SYSTEM,
            'SECTIONS' => 'Y',
            'SORT'     => 100,
            'LANG'     => [
                'ru' => [
                    'NAME' => 'Система',
                ],
                'en' => [
                    'NAME' => 'System',
                ],
            ],
        ]);

        if (empty($typeID)) {
            $error = \trim(\filter_var($type->LAST_ERROR, FILTER_SANITIZE_STRING));
            \yii\helpers\Console::printError($error);

            return false;
        }

        return true;
    }

    public function safeDown(): bool
    {
        \CIBlockType::Delete(Codex::TYPE_SYSTEM);

        return true;
    }
}
