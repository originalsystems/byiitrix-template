<?php

declare(strict_types=1);

use app\Codex;
use yii\db\Migration;

/** @noinspection LongInheritanceChainInspection */
class m170000_000000_main_configuration extends Migration
{
    public function safeUp(): bool
    {
        /** @var \CDBResult $siteObject */
        $siteObject = \CSite::GetByID(Codex::SITE_ID_MAIN);

        if ($siteObject->SelectedRowsCount() > 0) {
            $siteFields = [
                'ACTIVE'          => 'Y',
                'SORT'            => '1',
                'DEF'             => 'Y',
                'NAME'            => 'ProjectExample',
                'DIR'             => '/',
                'FORMAT_DATE'     => 'DD.MM.YYYY',
                'FORMAT_DATETIME' => 'DD.MM.YYYY HH:MI:SS',
                'SITE_NAME'       => 'ProjectExample',
                'SERVER_NAME'     => WEB_HOSTNAME,
                'EMAIL'           => 'info@' . WEB_HOSTNAME,
                'DOMAINS'         => \implode("\n", [
                    WEB_HOSTNAME,
                ]),
            ];

            $site = new \CSite();
            $site->Update(Codex::SITE_ID_MAIN, $siteFields);

            if (\strlen($site->LAST_ERROR) > 0) {
                $errors[] = $site->LAST_ERROR;
            }
        }

        $this->delete('b_site_template');
        $this->insert('b_site_template', [
            'SITE_ID'   => \app\Codex::SITE_ID_MAIN,
            'CONDITION' => '',
            'SORT'      => 1,
            'TEMPLATE'  => 'main',
        ]);

        \SetMenuTypes([
            'left'   => 'Левое меню',
            'top'    => 'Верхнее меню',
            'bottom' => 'Нижнее меню',
        ], Codex::SITE_ID_MAIN);

        return true;
    }

    public function safeDown(): bool
    {
        // These settings shouldn't be reverted.
        return true;
    }
}
