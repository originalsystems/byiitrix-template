<?php

declare(strict_types=1);

use yii\db\Migration;
use app\Codex;

/** @noinspection LongInheritanceChainInspection */
class m170000_000200_add_content_iblock_type extends Migration
{
    public function safeUp(): bool
    {
        $type   = new \CIBlockType();
        $typeID = $type->Add([
            'ID'       => Codex::TYPE_CONTENT,
            'SECTIONS' => 'Y',
            'SORT'     => 200,
            'LANG'     => [
                'ru' => [
                    'NAME' => 'Контент',
                ],
                'en' => [
                    'NAME' => 'Content',
                ],
            ],
        ]);

        if (empty($typeID)) {
            $error = \trim(\filter_var($type->LAST_ERROR, FILTER_SANITIZE_STRING));
            \yii\helpers\Console::printError($error);

            return false;
        }

        return true;
    }

    public function safeDown(): bool
    {
        \CIBlockType::Delete(Codex::TYPE_CONTENT);

        return true;
    }
}
