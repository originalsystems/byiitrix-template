<?php

declare(strict_types=1);

use yii\db\Migration;
use app\Codex;

/** @noinspection LongInheritanceChainInspection */
class m170000_000300_add_catalog_iblock_type extends Migration
{
    public function safeUp(): bool
    {
        $type   = new \CIBlockType();
        $typeID = $type->Add([
            'ID'       => Codex::TYPE_CATALOG,
            'SECTIONS' => 'Y',
            'SORT'     => 300,
            'LANG'     => [
                'ru' => [
                    'NAME' => 'Каталог',
                ],
                'en' => [
                    'NAME' => 'Catalog',
                ],
            ],
        ]);

        if (empty($typeID)) {
            $error = \trim(\filter_var($type->LAST_ERROR, FILTER_SANITIZE_STRING));
            \yii\helpers\Console::printError($error);

            return false;
        }

        return true;
    }

    public function safeDown(): bool
    {
        \CIBlockType::Delete(Codex::TYPE_CATALOG);

        return true;
    }
}
