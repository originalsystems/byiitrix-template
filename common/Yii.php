<?php

use yii\BaseYii;
use yii\di\Container;

require APP_ROOT . '/vendor/yiisoft/yii2/BaseYii.php';

class Yii extends BaseYii
{
    /**
     * @var \byiitrix\console\Application
     */
    public static $app;
    public static $aliases = [
        '@byiitrix' => APP_ROOT . '/vendor/originalsystems/byiitrix-kernel',
        '@console'  => APP_ROOT . '/console',
        '@common'   => APP_ROOT . '/common',
        '@local'    => APP_ROOT . '/public_html/local',
        '@yii'      => APP_ROOT . '/vendor/yiisoft/yii2',
    ];
}

\spl_autoload_register(['Yii', 'autoload'], true, true);

$coreMap   = require APP_ROOT . '/vendor/yiisoft/yii2/classes.php';
$extendMap = require APP_ROOT . '/vendor/originalsystems/byiitrix-kernel/classes.php';

\Yii::$classMap  = \array_merge($coreMap, $extendMap);
\Yii::$container = new Container();
