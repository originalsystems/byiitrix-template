<?php

declare(strict_types=1);

\error_reporting(0);

\define('APP_ROOT', \dirname(\dirname(\realpath(__DIR__))));
\define('WEB_ROOT', APP_ROOT . '/public_html');
\define('BX_CACHE_SID', \md5($_SERVER['HTTP_HOST'] . $_SERVER['DOCUMENT_ROOT']));
\define('BX_CUSTOM_TO_UPPER_FUNC', '\mb_strtoupper');
\define('BX_CUSTOM_TO_LOWER_FUNC', '\mb_strtolower');

\define('YII_DEBUG', (bool) \getenv('APP_DEBUG'));
\define('YII_ENV', \getenv('APP_ENV'));
\define('BIN_PHP', \getenv('BIN_PHP'));

\define('WEB_PROTOCOL', \getenv('WEB_PROTOCOL'));
\define('WEB_HOSTNAME', \getenv('WEB_HOSTNAME'));
\define('WEB_URL', WEB_PROTOCOL . '://' . WEB_HOSTNAME);

$_SERVER['SERVER_NAME'] = $_SERVER['HTTP_HOST'] = \preg_replace_callback('#^(?P<host>[^:]++):(?P<port>\d++)$#', function (array $matches) {
    return $matches['host'];
}, $_SERVER['HTTP_HOST'] ? : WEB_HOSTNAME);

if (\strpos($_SERVER['HTTP_HOST'], WEB_HOSTNAME) > 0) {
    \define('SUB_DOMAIN_PATH', \str_replace('.' . WEB_HOSTNAME, '', $_SERVER['HTTP_HOST']));
    \define('SUB_DOMAIN', \explode('.', SUB_DOMAIN_PATH)[0]);
} else {
    \define('SUB_DOMAIN_PATH', null);
    \define('SUB_DOMAIN', null);
}
