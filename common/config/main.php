<?php

use yii\db\Connection;
use yii\caching\FileCache;

global $DB;

$parts = \explode(':', $DB->DBHost);
$host  = $parts[0];
$port  = (isset($parts[1]) && \is_numeric($parts[1])) ? (int) $parts[1] : 3306;

return [
    'id'                  => \getenv('WEB_HOSTNAME'),
    'basePath'            => \dirname(__DIR__),
    'controllerNamespace' => 'console\controllers',
    'components'          => [
        'cache' => [
            'class'     => FileCache::class,
            'cachePath' => WEB_ROOT . '/bitrix/cache/yii',
            'fileMode'  => BX_FILE_PERMISSIONS,
            'dirMode'   => BX_DIR_PERMISSIONS,
        ],
        'db'    => [
            'class'               => Connection::class,
            'dsn'                 => "mysql:host={$host};port={$port};dbname={$DB->DBName}",
            'username'            => $DB->DBLogin,
            'password'            => $DB->DBPassword,
            'charset'             => 'utf8',
            'enableSchemaCache'   => true,
            'schemaCacheDuration' => 3600,
            'schemaCache'         => 'cache',
        ],
    ],
    'language'            => 'ru',
    'name'                => \getenv('WEB_HOSTNAME'),
    'params'              => [],
    'vendorPath'          => APP_ROOT . '/vendor',
    'runtimePath'         => APP_ROOT . '/common/runtime',
];
