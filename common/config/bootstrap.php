<?php

declare(strict_types=1);

if (\class_exists('Kint')) {
    \Kint\Kint::$enabled_mode = !YII_ENV_PROD;
}
